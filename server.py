#!/usr/bin/env python

# WS server example that synchronizes state across clients

import asyncio
import json
import logging
import websockets

logging.basicConfig()

STATE = {"value": 0,
    "client-1": 0,
    "client-2": 0,
    "client-3": 0,
    }


USERS = set()


def state_event():
    return json.dumps({"type": "state", **STATE})


def users_event():
    return json.dumps({"type": "users", "count": len(USERS)})


async def notify_state():
    if USERS:  # asyncio.wait doesn't accept an empty list
        message = state_event()
        await asyncio.wait([user.send(message) for user in USERS])


async def notify_users():
    if USERS:  # asyncio.wait doesn't accept an empty list
        message = users_event()
        await asyncio.wait([user.send(message) for user in USERS])


async def register(websocket):
    USERS.add(websocket)
    await notify_users()


async def unregister(websocket):
    USERS.remove(websocket)
    await notify_users()


async def counter(websocket, path):
    # register(websocket) sends user_event() to websocket
    await register(websocket)
    try:
        await websocket.send(state_event())
        async for message in websocket:
            data = json.loads(message)
            if data["action"] == "minus":
                STATE["value"] -= 1
                await notify_state()
            elif data["action"] == "plus":
                STATE["value"] += 1
                await notify_state()

            # test-start
            elif data["action"] == 'client-1-minus':
                STATE["client-1"] -= 1
                await notify_state()
            elif data["action"] == 'client-1-plus':
                STATE["client-1"] += 1
                await notify_state()

            elif data["action"] == 'client-2-minus':
                STATE["client-2"] -= 1
                await notify_state()
            elif data["action"] == 'client-2-plus':
                STATE["client-2"] += 1
                await notify_state()

            elif data["action"] == 'client-3-minus':
                STATE["client-3"] -= 1
                await notify_state()
            elif data["action"] == 'client-3-plus':
                STATE["client-3"] += 1
                await notify_state()
            # test-end
            else:
                logging.error("unsupported event: {}", data)
    finally:
        await unregister(websocket)


start_server = websockets.serve(counter, "localhost", 6789)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()